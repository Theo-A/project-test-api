const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

class Authentification {

    static generateAccessToken(username) {
        return jwt.sign(username, process.env.JWT_SECRET, {expiresIn: '1800s'});
    }

    static authenticateToken(req, res, next) {
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]

        if (token == null) {
            return res.sendStatus(401);
        }

        jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
            if (err) {
                console.error(err);
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        })
    }

}

module.exports = Authentification;

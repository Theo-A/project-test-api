const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();
const Authentification = require('../middlewares/authentification');

module.exports = function ({User}) {
    router.get('/', Authentification.authenticateToken, async (req, res) => {
        res.json(await User.findAll());
    });

    router.post('/', async (req, res) => {
        if (!req.body.username) {
            return res.status(400).send('Bad request');
        }

        try {
            const user = await User.create({username: req.body.username});

            res.status(201).json(user);
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.get('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const user = await User.findByPk(id);

            if (user) {
                res.json(user);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.delete('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const deleted = await User.destroy({where: {id}});

            if (deleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.put('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const [updated, [user]] = await User.update(req.body, {where: {id}, returning: true});

            if (updated) {
                res.json(user);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.post('/login', async (req, res) => {
        try {
            const user = await User.findOne({where: {username: req.body.username}});

            if (!user)
                res.status(400).json({username: 'Username not found',});
            else if (!bcrypt.compareSync(req.body.password, user.password)) {
                res.status(400).json({password: 'Password is incorrect',});
            } else {
                res.json({token: Authentification.generateAccessToken({username: req.body.username})});
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    return router;
}

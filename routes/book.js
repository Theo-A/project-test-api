const express = require('express');
const router = express.Router();
const Authentification = require('../middlewares/authentification');
const {ValidationError} = require('sequelize');

module.exports = function ({Book}) {
    router.get('/', Authentification.authenticateToken, async (req, res) => {
        res.json(await Book.findAll());
    });

    router.post('/', Authentification.authenticateToken, async (req, res) => {
        if (!req.body.name || !req.body.price || !req.body.authorId || !req.body.summary) {
            return res.status(400).send('Bad request');
        }

        try {
            const book = await Book.create({
                name: req.body.name,
                price: req.body.price,
                summary: req.body.summary,
                authorId: req.body.authordId
            });

            res.status(201).json(book);
        } catch (error) {
            if (error instanceof ValidationError) {
                return res.status(400).json(error.errors[0].message);
            } else if (error.name === 'SequelizeForeignKeyConstraintError') {
                return res.status(400).json(error.detail);
            }
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.get('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const book = await Book.findByPk(id);

            if (book) {
                res.json(book);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.delete('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const deleted = await Book.destroy({where: {id}});

            if (deleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.put('/:id', Authentification.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const [updated, [book]] = await Book.update(req.body, {where: {id}, returning: true});

            if (updated) {
                res.json(book);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    return router;
}

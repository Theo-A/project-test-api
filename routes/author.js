const express = require('express');
const router = express.Router();
const Middleware = require('../middlewares/authentification');

module.exports = function ({Author}) {
    router.get('/', Middleware.authenticateToken, async (req, res) => {
        res.json(await Author.findAll());
    });

    router.post('/', Middleware.authenticateToken, async (req, res) => {
        if (!req.body.name) {
            return res.status(400).send('Bad request');
        }

        try {
            const author = await Author.create({name: req.body.name});

            res.status(201).json(author);
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.get('/:id', Middleware.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const author = await Author.findByPk(id);

            if (author) {
                res.json(author);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.delete('/:id', Middleware.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const deleted = await Author.destroy({where: {id}});

            if (deleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.put('/:id', Middleware.authenticateToken, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const [updated, [author]] = await Author.update(req.body, {where: {id}, returning: true});

            if (updated) {
                res.json(author);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    return router;
}

const {Sequelize} = require('sequelize');

class Database {

    static async init() {
        const sequelize = new Sequelize('postgres://user:password@localhost:5438/api', {logging: false});

        try {
            await sequelize.authenticate();
        } catch (error) {
            console.error('Unable to connect to the database:', error);
        }

        return sequelize;
    }

}

module.exports = Database;

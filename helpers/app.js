const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

class App {

    static init(app, sequelize) {
        // Models
        const User = require('../models/user')(sequelize);
        const Author = require('../models/author')(sequelize);
        const Book = require('../models/book')(sequelize);

        // Relations
        Book.belongsTo(Author);
        Author.hasMany(Book);

        // Routers
        const userRouter = require('../routes/user')({User});
        const authorRouter = require('../routes/author')({Author});
        const bookRouter = require('../routes/book')({Book});

        app.disable('x-powered-by');

        app.use(cors());
        app.use(bodyParser.urlencoded({extended: true}));
        app.use(bodyParser.json())
        app.use('/static', express.static(__dirname + '/../public'));


        app.use('/api/users', userRouter);
        app.use('/api/authors', authorRouter);
        app.use('/api/books', bookRouter);

        return app;
    }

    static server(app) {
        const port = process.env.PORT || 3000;
        app.listen(port, () => console.info(`Platform listening at http://localhost:${port}`));
    }

}

module.exports = App;

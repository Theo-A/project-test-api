const {Model, DataTypes} = require('sequelize');

module.exports = function (sequelize) {
    class Author extends Model {
    }

    Author.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {sequelize, modelName: 'author'});

    return Author;
};

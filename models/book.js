const {Model, DataTypes} = require('sequelize');

module.exports = function (sequelize) {
    class Book extends Model {
    }

    Book.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false,
            validate: {
                min: 0,
            }
        },
        summary: {
            type: DataTypes.TEXT,
            allowNull: false,
        }
    }, {sequelize, modelName: 'book'});

    return Book;
};

const {Model, DataTypes} = require('sequelize');
const bcrypt = require('bcryptjs');

module.exports = function (sequelize) {
    class User extends Model {
    }

    User.init({
        username: {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {sequelize, modelName: 'user'});

    const encodePassword = (user) => {
        user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
    };

    User.addHook('beforeCreate', encodePassword);
    User.addHook('beforeUpdate', encodePassword);

    return User;
};

const {BeforeAll, AfterAll, Given, Then, When} = require('@cucumber/cucumber');
const express = require('express');
const {expect} = require('expect');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const supertest = require('supertest');
const userFixture = require('../tests/fixtures/user.fixture');
const authorFixture = require('../tests/fixtures/author.fixture');
const bookFixture = require('../tests/fixtures/book.fixture');

let sequelize, request, references, user, author, book;

BeforeAll(async function () {
    sequelize = await Database.init();
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    // Fixtures
    user = await userFixture(sequelize);
    author = await authorFixture(sequelize);
    book = await bookFixture(sequelize);

    // References
    references = {};
    references['author.id'] = author.id;
    references['book.id'] = book.id;

    request = supertest(App.init(express(), sequelize));

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'Theo',
        password: '123'
    });

    user.token = response.body.token;
});

AfterAll(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
    sequelize.close();
});

Given('I have reference {string}', function (reference) {
    this.references = [];
    this.references.push(reference);
});

Given('I have payload', function (dataTable) {
    dataTable.rawTable = dataTable.rawTable.map(item => {
        if (this.references.includes(item[1])) {
            item[1] = references[item[1]];
        }

        if (!isNaN(item[1])) {
            item[1] = parseInt(item[1]);
        }

        return item;
    });

    this.payload = dataTable.rowsHash();
});

When('I request {string} {string} with payload', async function (method, path) {
    this.response = await request[method.toLowerCase()](path)
        .set('Authorization', 'Bearer ' + user.token)
        .set('Content-Type', 'application/json')
        .send(this.payload);
});

When('I request {string} {string}', async function (method, path) {
    this.response = await request[method.toLowerCase()](path)
        .set('Authorization', 'Bearer ' + user.token)
        .set('Content-Type', 'application/json')
        .send();
});

Then('The response status should be {int}', function (statusCode) {
    expect(this.response.status).toBe(statusCode);
});

Then('I should have an object with the following attributes', function (dataTable) {
    dataTable.rawTable = dataTable.rawTable.map(item => {
        if (this.references.includes(item[1])) {
            item[1] = references[item[1]];
        }

        if (!isNaN(item[1])) {
            item[1] = parseInt(item[1]);
        }

        return item;
    });

    const expected = dataTable.rowsHash();
    const actual = this.response.body;

    expect(typeof actual).toBe('object');
    Object.keys(expected).forEach(key => expect(actual).toHaveProperty(key, expected[key]));
});

Then('I should have the {string} attribute', function (attr) {
    expect(this.response.body).toHaveProperty(attr);
});

Then('I should have an object with a length of {int}', function (int) {
    expect(Object.keys(this.response.body)).toHaveLength(int);
});

Then('I should have an object with {int} elements', function (int) {
    expect(Object.keys(this.response.body)).toHaveLength(int);
});

Given('I should have an empty object', function () {
    expect(Object.keys(this.response.body)).toHaveLength(0);
});

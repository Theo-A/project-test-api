Feature: Book

  Scenario: I should see all books
    When I request 'GET' '/api/books'
    Then I should have an object with a length of 1
    And I should have an object with 1 elements

  Scenario: I should create a new book
    Given I have reference 'author.id'
    And I have payload
      | name    | Book |
      | price   | 100       |
      | authorId | author.id  |
      | summary | Description of the book  |
    When I request 'POST' '/api/books' with payload
    Then The response status should be 201
    And I should have the 'id' attribute
    And I should have an object with the following attributes
      | name    | Book |
      | price   | 100       |
      | authorId | author.id  |
      | summary | Description of the book  |

  @Fixture
  Scenario: I should not retrieve the book unknown
    When I request 'GET' '/api/books/-1'
    Then The response status should be 404
    And I should have an empty object

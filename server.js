const express = require('express');
const app = express();

(async () => {
    const Database = require('./helpers/database');
    const sequelize = await Database.init();
    const App = require('./helpers/app');

    App.init(app, sequelize);
    App.server(app);
})();

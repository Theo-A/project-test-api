const supertest = require('supertest');
const express = require('express');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const userFixture = require('./fixtures/user.fixture');
const authorFixture = require('./fixtures/author.fixture');
const bookFixture = require('./fixtures/book.fixture');

let sequelize, request, user, author, book;

beforeAll(async () => {
    sequelize = await Database.init();

    // Request
    request = supertest(App.init(express(), sequelize));
});

beforeEach(async () => {
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    // Fixtures
    user = await userFixture(sequelize);
    author = await authorFixture(sequelize);
    book = await bookFixture(sequelize);

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'Theo',
        password: '123'
    });

    user.token = response.body.token;
});

afterEach(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
});

afterAll(async () => {
    sequelize.close();
});

const authenticatedRequest = (endpoint, method = 'get') => {
    return request[method](endpoint).set('Authorization', 'Bearer ' + user.token);
}

describe('Books', () => {
    it('should return all books', async () => {
        const response = await authenticatedRequest('/api/books').send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(1);
    });

    it('Can\'t show all books without token', async () => {
        const response = await request.get('/api/books').send();

        expect(response.status).toBe(401);
    });

    it('should create a book', async () => {
        let response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Game of Thrones',
            price: 100,
            authorId: author.id,
            summary: "Jeu des trones"
        });

        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Game of Thrones');
        expect(response.body).toHaveProperty('price', 100);
    });

    it('Can\'t create a book without author', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            price: 100,
            summary: "l'anneau"
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t create a book without summary', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            price: 100,
            authorId: 1
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t create a book with invalid author', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            price: 100,
            authorId: undefined,
            summary: "l'anneau"
            
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t be create a book with invalid price', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            price: -100,
            authorId: author.id,
            summary: "l'anneau"
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t create a book without price', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            authorId: author.id,
            summary: "l'anneau"
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t create a book without name', async () => {
        const response = await authenticatedRequest('/api/books', 'post').set('Content-Type', 'application/json').send({
            price: 100,
            authorId: author.id,
            summary: "l'anneau"
        });

        expect(response.status).toBe(400);
    });

    it('Can\'t create a book without token', async () => {
        const response = await request.post('/api/books').set('Content-Type', 'application/json').send({
            name: 'Le seigneur des anneaux',
            price: 100,
            authorId: author.id,
            summary: "l'anneau"
        });

        expect(response.status).toBe(401);
    });

    it('should has book', async () => {
        const response = await authenticatedRequest('/api/books/' + book.id).send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Harry Potter');
        expect(response.body).toHaveProperty('price', 42);
        expect(response.body).toHaveProperty('summary', "Un jeune sorcier...");
    });

    it('Can\'t show a book if it doesn\'t exist', async () => {
        const response = await authenticatedRequest('/api/books/' + book.id + 1).send();

        expect(response.status).toBe(404);
    });

    it('Can\'t show a book without token', async () => {
        const response = await request.get('/api/books/' + book.id).send();

        expect(response.status).toBe(401);
    });

    it('should update a book', async () => {
        const response = await authenticatedRequest('/api/books/' + book.id, 'put').set('Content-Type', 'application/json').send({
            name: 'Les Animaux Fantastiques'
        });

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Les Animaux Fantastiques');       
        expect(response.body).toHaveProperty('summary', 'Un jeune sorcier...');
    });

    it('Can\'t update a book if it doesn\'t exist', async () => {
        const response = await authenticatedRequest('/api/books/' + book.id + 1, 'put').set('Content-Type', 'application/json').send({
            name: 'Les Animaux Fantastiques'
        });

        expect(response.status).toBe(404);
    });

    it('Can\'t update a book without token', async () => {
        const response = await request.put('/api/books/' + book.id).set('Content-Type', 'application/json').send({
            name: 'Les Animaux Fantastiques'
        });

        expect(response.status).toBe(401);
    });

    it('should remove a book', async () => {
        const response = await authenticatedRequest('/api/books/' + book.id, 'delete').send()

        expect(response.status).toBe(204);
    });

    it('Can\'t delete a book without token', async () => {
        const response = await request.delete('/api/books/' + book.id).send()

        expect(response.status).toBe(401);
    });
});

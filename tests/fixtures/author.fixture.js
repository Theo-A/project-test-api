module.exports = async function (sequelize) {
    const Author = require('../../models/author')(sequelize);

    try {
        return await Author.create({name: 'JK Rolling'});
    } catch (error) {
        console.error(error);
        process.exit();
    }
}

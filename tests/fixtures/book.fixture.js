module.exports = async function (sequelize) {
    const Book = require('../../models/book')(sequelize);

    try {
        return await Book.create({name: 'Harry Potter', price: 42, authorId: 1, summary: "Un jeune sorcier..."});
    } catch (error) {
        console.error(error);
        process.exit();
    }
}

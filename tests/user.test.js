const supertest = require('supertest');
const express = require('express');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const userFixture = require('./fixtures/user.fixture');

let sequelize, request, user;

beforeAll(async () => {
    sequelize = await Database.init();

    // Request
    request = supertest(App.init(express(), sequelize));
});

beforeEach(async () => {
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    // Fixtures
    user = await userFixture(sequelize);

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'Theo',
        password: '123'
    });

    user.token = response.body.token;
});

afterEach(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
});

afterAll(async () => {
    sequelize.close();
});

describe('Users', () => {
    it('can login', async () => {
        const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
            username: 'Theo',
            password: '123'
        });

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('token');
    });
});

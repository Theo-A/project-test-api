const supertest = require('supertest');
const express = require('express');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const userFixture = require('./fixtures/user.fixture');
const authorFixture = require('./fixtures/author.fixture');

let sequelize, request, user, author;

beforeAll(async () => {
    sequelize = await Database.init();

    // Request
    request = supertest(App.init(express(), sequelize));
});

beforeEach(async () => {
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    // Fixtures
    user = await userFixture(sequelize);
    author = await authorFixture(sequelize);

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'Theo',
        password: '123'
    });

    user.token = response.body.token;
});

afterEach(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
});

afterAll(async () => {
    sequelize.close();
});

const authenticatedRequest = (endpoint, method = 'get') => {
    return request[method](endpoint).set('Authorization', 'Bearer ' + user.token);
}

describe('Authors', () => {
    it('should get all authors', async () => {
        const response = await authenticatedRequest('/api/authors').send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(1);
    });

    it('should create a author', async () => {
        const response = await authenticatedRequest('/api/authors','post').set('Content-Type', 'application/json').send({
            name: 'JRR Martin',
        });

        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'JRR Martin');
    });

    it('should has author', async () => {
        const response = await authenticatedRequest('/api/authors/' + author.id).send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'JK Rolling');
    });

    it('should update an author', async () => {
        const response = await authenticatedRequest('/api/authors/' + author.id,'put').set('Content-Type', 'application/json').send({
            name: 'JRR Tolkien',
        });

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'JRR Tolkien');
    });

    it('should remove a author', async () => {
        const response = await authenticatedRequest('/api/authors/' + author.id,'delete').send()

        expect(response.status).toBe(204);
    });
});

(async () => {
    const Database = require('./helpers/database');
    const sequelize = await Database.init();

    require('./models/user')(sequelize);
    const Author = require('./models/author')(sequelize);
    const Book = require('./models/book')(sequelize);

    Book.belongsTo(Author);
    Author.hasMany(Book);

    await sequelize.sync({alter: true});
    await sequelize.close();
})();
